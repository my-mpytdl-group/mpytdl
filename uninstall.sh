#!/bin/bash


if [ -f /etc/mpytdl/mpytdl.sh ]; then

	sudo rm -r /etc/mpytdl/
	sed -i "/alias mplayer.*\+/d" ~/.bashrc
	source ~/.bashrc
	echo -e "\e[32;1m::\e[37m Reinicie el emulador de terminal para culminar la desinstalación.\e[0m"

fi
