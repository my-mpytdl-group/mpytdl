#!/bin/bash

sudo mkdir -p /etc/mpytdl
sudo cp -u mpytdl.sh /etc/mpytdl/mpytdl.sh
sudo chmod u+x /etc/mpytdl/mpytdl.sh

# Insertar línea en un fichero.
insert_line (){

	if [[ `echo "$2" | egrep -o "^/"` ]]; then

		echo "$1" | sudo tee -a $2 >&2 /dev/null
		#sudo sh -c "$1" >> $2
	else
		echo "$1" >> $2
	fi

 	echo -e "\e[34;1m  -->\e[37m Se insertó correctamente la línea en el fichero '\e[32m$2\e[37m'.\e[0m"

}

# Obtener existencia única de una línea en un fichero.
uniq_existence (){

	declare -a list=("$@")	

	# Fichero de configuración.
        FILE="${list[$(($#-1))]}"

	# Si el fichero existe.
	if [ -f $FILE ]; then

		for line in "$@"
		do

			# Si la línea existe en el fichero.
			if [[ $(grep -ow "$line" $FILE) ]]; then

				# Mientras hayan más de una existencia de la línea.	
				while [ $(grep -wc "$line" $FILE) -gt 1  ]
				do

					# Si la línea contiene el caracter especial /.	
					if [[ `echo "$line" | egrep -o "/"` ]]; then

						# Sustituir \\ por nada.
						line=$(echo $line | sed "s/\\\//g")

						# Sustituir \ por \/. 
						line=$(echo $line | sed "s/\//\\\\\//g")

						# Obtengo número de la última línea repetida.
						repeatLine=$(sed -n "/${line}/=" $FILE | tail -1)

						# Sustituir \/ por /.
						line=$(echo $line | sed "s/\\//\//g")
					else
						# Obtengo número de la última línea repetida.
						repeatLine=$(sed -n "/${line}/=" $FILE | tail -1)
					fi

					if [[ $repeatLine != "" ]]; then

						# Elimino la línea repetida.	
						sed -i ${repeatLine}d $FILE
 						echo -e "\e[34;1m ->\e[37m Se eliminó la línea repetida $repeatLine.\e[0m"
					fi

				done

			else
				if [ ! -f "$line" ]; then

					insert_line "$line" $FILE
				fi
			fi
		done

		echo -e "\e[32;1m::\e[37m Fichero '\e[32m${FILE}\e[37m' establecido con líneas no repetidas.\e[0m"
	else
		echo -e "\e[33;1mWarning:\e[37m Fichero '\e[32m${FILE}\e[37m' no existe. \e[0m"
	fi	
}



# líneas a insertar.
declare -a bashrc=(
"alias mplayer='/etc/mpytdl/mpytdl.sh'"
)

uniq_existence "${bashrc[@]}" ~/.bashrc
source ~/.bashrc

# Insertar resolución preferida entre HD y FHD.
insert_resolution() {

	if [[ $1 -eq 1 ]]; then

		if [[ `grep "RESOLUTION=720" /etc/mpytdl/mpytdl.sh` ]]; then

			echo "Resolución previamente establecida en 720p."
		else
			sudo sed -i "s/RESOLUTION=1080/RESOLUTION=720/" /etc/mpytdl/mpytdl.sh
			echo "Resolución HD 1080x720p establecida."
		fi
	else
		if [[ `grep "RESOLUTION=1080" /etc/mpytdl/mpytdl.sh` ]]; then

			echo "Resolución previamente establecida en 1080p."
		else
			sudo sed -i "s/RESOLUTION=720/RESOLUTION=1080/" /etc/mpytdl/mpytdl.sh
			echo "Resolución FHD 1920x1080p establecida."
		fi
	fi


}

PS3="Seleccione resolución de video a descargar: "
select var in "HD 1280x720" "FHD 1920x1080"
do
	case $REPLY in
		1) insert_resolution 1
		break;;

		2) insert_resolution 2
		break;;

		*) echo "Opción inválida."
		;;
	esac
done


echo -e "\e[32;1m::\e[37m Reinicie su emulador de terminal para culminar la instalación.\e[0m"
